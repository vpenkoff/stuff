#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

static time_t firsttime = 0;

void init_time(void)
{
	firsttime = time(NULL);
}

#define	SECSPERMIN	60

int
main(int argc, char **argv)
{
	init_time();
	for (;;) {
		time_t lasttime = time(NULL);
		if (lasttime < firsttime + SECSPERMIN) {
			continue;
		} else {
			printf("KOOOR\n");
			return(0);
		}
	}	
	return (0);
}
