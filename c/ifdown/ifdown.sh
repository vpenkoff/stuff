#!/usr/bin/sh

PORT1="port-3/1"
PORT2="port-3/2"

while [ 1 ]
do
	ifconfig $PORT1 down
	sleep 1
	ifconfig $PORT1 up
	sleep 1
	ifconfig $PORT2 down
	sleep 1
	ifconfig $PORT2 up
	sleep 1
done
