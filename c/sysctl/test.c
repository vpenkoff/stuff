#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <errno.h>
#include <strings.h>
#include <net/route.h>
#include <string.h>
#include <unistd.h>
#include <sys/uio.h>
#include <netinet/in.h>
#include <sys/sysctl.h>
#include <sys/un.h>
#include <fcntl.h>

int 
main(int argc, char **argv)
{ 	
	size_t  needed;
        int     mib[6];
        char    *buf, *next, *lim;

        mib[0] = CTL_NET;
        mib[1] = AF_ROUTE;
        mib[2] = 0;
        mib[3] = 0;
        mib[4] = NET_RT_DUMP;
        mib[5] = 0;

        if (sysctl(mib, 6, NULL, &needed, NULL, 0) < 0) {
                printf("%d, %s\n", __LINE__, __func__);
                return(-1);
        }

        if ((buf = malloc(needed)) == 0) {
                printf("%d, %s\n", __LINE__, __func__);
                return(-1);
        }

        if (sysctl(mib, 6, buf, &needed, NULL, 0) < 0) {
                printf("%d, %s\n", __LINE__, __func__);
                return(-1);
        }

	return (0);
}
 
