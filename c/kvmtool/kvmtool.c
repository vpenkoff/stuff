#include <err.h>
#include <errno.h>
#include <kvm.h>
#include <limits.h>
#include <nlist.h>
#include <paths.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/socketvar.h>
#include <sys/sysctl.h>

#include <net/if.h>
#include <net/if_var.h>
#include <net/if_types.h>
#include <net/ethernet.h>
#include <netinet/in.h>
#include <netinet/in_var.h>
#include <arpa/inet.h>

static struct nlist nl[] = {
#define N_IFNET		0
	{ .n_name = "_ifnet" }
};

struct entry {
	char 			*ifname;
	struct entry		*next;
};

struct entry *next = NULL;
struct entry *root = NULL;

int 	kread(u_long, void *, size_t);
void	fetch_ifp(u_long ifnetaddr);
void	if_insert(struct ifnet *);
void	if_show(void);
void	if_deallocate(void);

LIST_HEAD(entryhead, entry) head;


static kvm_t *kvmd;
static char *nlistf = NULL;

int
kread(u_long addr, void *buf, size_t size)
{
	char errbuf[_POSIX2_LINE_MAX];

	if (kvmd == NULL) {
		kvmd = kvm_openfiles(nlistf, NULL, NULL, O_RDONLY, errbuf);
		setgid(getgid());
		if (kvmd != NULL) {
			if (kvm_nlist(kvmd, nl) < 0) {
				if (nlistf)
					errx(1, "%s: kvm_nlist: %s", nlistf,
					     kvm_geterr(kvmd));
				else
					errx(1, "kvm_nlist: %s", kvm_geterr(kvmd));
			}

			if (nl[0].n_type == 0) {
				if (nlistf)
					errx(1, "%s: no namelist", nlistf);
				else
					errx(1, "no namelist");
			}
		} else {
			warnx("kvm not available: %s", errbuf);
			return(-1);
		}
	}
	if (!buf)
		return (0);
	if (kvm_read(kvmd, addr, buf, size) != (ssize_t)size) {
		warnx("%s", kvm_geterr(kvmd));
		return (-1);
	}
	return (0);
}

void
fetch_ifp(u_long ifnetaddr)
{
	struct ifnet ifnet;
	struct ifnethead ifnethead;

	if (ifnetaddr == 0) {
		printf("ifnet: symbol not defined\n");
		return;
	}
	 
	if (kread(ifnetaddr, (char *)&ifnethead, sizeof ifnethead) != 0)
		return;
	ifnetaddr = (u_long)TAILQ_FIRST(&ifnethead);

	do {
		if (kread(ifnetaddr, (char *)&ifnet, sizeof (ifnet)) != 0)
			return;
	//	printf("%s\n", ifnet.if_xname);
		if_insert(&ifnet);
		ifnetaddr = (u_long)TAILQ_NEXT(&ifnet, if_link);
	} while (ifnetaddr);
}

void
if_insert(struct ifnet *ifp)
{
		struct entry *ptr;
		if (root == NULL) {
			if ((root = malloc(sizeof(struct entry))) == NULL) {
				fprintf(stderr, "KOR malloc");
				return;
			}
			root->ifname = ifp->if_xname;
			root->next = NULL;
		} else {
			ptr = root;
			while (ptr->next != NULL)
				ptr = ptr->next;
			if ((ptr->next = malloc(sizeof(struct entry))) == NULL) {
				fprintf(stderr, "KOR malloc");
				return;
			}

			ptr = ptr->next;
			ptr->ifname = ifp->if_xname;
			ptr->next = NULL;
		}
}

void
if_show(void)
{
	struct entry *ptr;
	if (root == NULL) {
		fprintf(stderr, "KOR show");
			return;
	} else {
		ptr = root;
		while (ptr->next != NULL) {
			printf("Interface: %s\n", ptr->ifname); 
			ptr = ptr->next;
		}
	}
}

void 
if_deallocate(void)
{
	struct entry *ptr, *ptr1;
	if (root == NULL) 
		fprintf(stderr, "KOR delete");
			return;

	ptr = root;
	while (ptr != NULL) {
		ptr1 = ptr;
		ptr = ptr->next;
		free(ptr1);
	}
}

int
main(int argc, char **argv)
{
	struct kevent change;
	struct kevent event;
	int kq, nev;

	LIST_INIT(&head);

	kread(0, NULL, 0);

	if ((kq = kqueue()) == -1) {
		perror("cannot create event q");
		return (-1);
	}

	EV_SET(&change, 1, EVFILT_TIMER, EV_ADD | EV_ENABLE, 0, 2000, 0);
	kread(0, NULL, 0);
	for (;;) {
		nev = kevent(kq, &change, 1, &event, 1, NULL);
		
		if (nev < 0) {
			perror("kevent error");
			return (-1);
		} else if (nev > 0) {
			if (event.flags & EV_ERROR) {
				fprintf(stderr, "EV_ERROR: %s\n",
				    strerror(event.data));
				return (-1);
			}
		}
		fetch_ifp(nl[N_IFNET].n_value);
		if_show();
		if_deallocate();
	}

	return (0);
}
