#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 20

/* The structure holding an ID */
struct elem {
	char num[10];
	struct elem *next;
};

struct elem *next = NULL;
struct elem *root = NULL;

/* Function definitions */

/*Function to help how to use the program */
void
usage(void)
{
	fprintf(stderr, "Usage: \n"
	    "\tblacklist [data file] [pattern file] \n"
	    "\twhere [data file] is the file containing all numbers \n"
	    "\tand [pattern] is the string for blacklisting\n");
}

/*Function to show the current list status */

void
printList(void)
{
	struct elem *ptr = root;

	while(ptr != NULL) {
		printf("Filtered num: %s\n", ptr->num);
		ptr = ptr->next;
	}
}

/*Function to clear the list (free memory) after the list is dumped*/

void
clearList(void)
{
	struct elem *ptr, *ptr1;

	if(root != NULL) {
		ptr = root->next;
		while(ptr != NULL) {
			ptr1 = root->next->next;
			root->next = ptr1;
			free(ptr);
			ptr = ptr1;
		}

		free(root->next);
		free(root);
		root = NULL;
	} else 
		fprintf(stderr, "No Elements in the List\n");
}

/* Function to dump the list on the hdd */

void
dumpList(void)
{
	struct elem *ptr = root;
	FILE *dump;

	dump = fopen("dump", "w+");
	if (dump == NULL) {
		fprintf(stderr, "Cannot write to file\n");
	}

	while(ptr != NULL) {
		fputs(ptr->num, dump);
		fputs("\n", dump);
		ptr = ptr->next;
	}

	fclose(dump);
}

/*Function to insert an element in the list */

int
insert_elem(char *str)
{
	struct elem *ptr;
	
	if (root == NULL) {
		if ((root = malloc(sizeof(struct elem))) == NULL) {
			fprintf(stderr, "Malloc error\n");
			return(-1);
		}
		strncpy(root->num, str, sizeof(root->num));
		root->next = NULL;
	} else {
		ptr = root;
		while(ptr->next != NULL)
			ptr = ptr->next;
		if ((ptr->next = malloc(sizeof(struct elem))) == NULL) {
			fprintf(stderr, "Malloc error\n");
			return(-1);
		}
		ptr = ptr->next;
		strncpy(ptr->num, str, sizeof(ptr->num));
		ptr->next = NULL;
	}
	return (0);	 
}

/* Function to check if a given number is blacklisted. 
 *Depend on the given blacklisting string 
*/

int
black_check(char *str, char *pattern)
{
	int i, j = 0;
	int len = strlen(pattern);

	for (i = 0; i < strlen(pattern); i++) {
		if(pattern[i] == str[i]) 
			j++;
	}

	return 1 ? j == len : 0;
}

/* Helper function to get the blacklisting pattern */

char *
getPattern(char *str)
{
	FILE *fp = fopen(str, "r");
	char pattern[MAX];
	char *ptr = NULL;
	fgets(pattern, MAX, fp);
	
	ptr = &pattern[0];
	return ptr;
}

/* Helper function to count the lines of the data file provided */

int
getLinesCount(char *data_file)
{
	FILE *output;
	char cmd[20];
	char data[512];
	int lines;
	
	strcpy(cmd, "wc -l ");
	strcat(cmd, data_file);

	output = popen(cmd, "r");

	if (!output) {
		fprintf(stderr, "Can't get output from the pipe");
		return (-1);
	}

	fgets(data, MAX, output);
	lines = atoi(&data[0]);

	if (pclose(output) != 0) 
		fprintf(stderr, "Error closing the pipe ");

	return (lines);
}

/* Function to read from the data file provided, based on Dynamic Array/Table. 
 * The line readed is saved as a string.
 * At the end we check if the readed string is blacklisted or not.
 */

int
readData(int *count, char *pattern)
{
	FILE *rfile;
	int max_line_len = 16;
	char **words;
	int lines = *count;
	

	words = (char **)malloc(sizeof(char *) * lines);
	if (words == NULL) {
		fprintf(stderr, "Out of mem\n");
		return (-1);
	}

	rfile = fopen("data", "r");
	if (rfile == NULL) {
		fprintf(stderr, "Error opening file\n");
		return (-1);
	}

	int i;
	for (i=0;1;i++) {
		int j;

		if (i >= lines) {
			int new_size;

			/* Double our allocation and re-allocate (Dynamic array) */
			new_size = lines * 2;
			words = (char **)realloc(words, sizeof(char *) * new_size);
			if (words == NULL) {
				fprintf(stderr, "Out of mem\n");
				return (-1);
			}
			lines = new_size;
		}
		/* Allocate space for the next line */
		words[i] = malloc(max_line_len);

		if (words[i] == NULL) {
			fprintf(stderr, "Out of mem\n");
			return (-1);
		}

		if (fgets(words[i], max_line_len - 1, rfile) == NULL)
			break;

		/* Remove line feed and carriage return  */
		for (j = strlen(words[i])-1; j >= 0 && 
				(words[i][j] == '\n' || words[i][j] == '\r'); j--)
			;
		words[i][j]='\0';
	}

	int j;
	for (j = 0; j < i; j++) {
		if(black_check(words[j], pattern) == 1) {
			printf("%s Blacklisted\n", words[j]);
		} else 
			insert_elem(words[j]);
	}
	
	/* Free memory after the job is done */
	for (; i>=0; i--)
		free(words[i]);
	free(words);
	
	return (0);
}
