#ifndef BLACKLIST_H
#define BLACKLIST_H

/* function declaration */
extern void	printList(void);
extern void 	dumpList(void);
extern void	clearList(void);
extern int 	insert_elem(char *str);
extern int	black_check(char *str, char *pattern);
extern void	usage(void);
extern char	*getPattern(char *str);
extern int	getLinesCount(char *data_file);
extern int	readData(int *count, char *pattern);

struct elem;
#endif
