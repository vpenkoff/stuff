/* The given solution provides a way to filter blacklisted ID's
 * and store the new list on the hdd. The way used is a simple linked list.
 * Before an insertion occured, check the candidate if it is to be blacklisted 
 * or not.  
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "blacklist.h"

int
main(int argc, char **argv)
{

	int lines;
	char *pattern;

	if (argc < 3) {
		usage();
		return (-1);
	}

	lines = getLinesCount(argv[1]);
	pattern = argv[2];
	readData(&lines, pattern);
	printf("#################\n");
	printList();
	dumpList();
	clearList();
	
	return (0);
}
