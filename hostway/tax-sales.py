from enum import Enum
import sys

class Tax(object):
	def __init__(self, taxrate):
		self.__rate = taxrate
	
	def taxitem(self, price):
		tax = price * self.__rate
		return tax
	
	@staticmethod		
	def round_to(n, precission):
		correction = 0.5 if n >= 0 else -0.5
		return int(n/precission+correction)*precission
		
class BasicTax(Tax):
	def __init__(self):
		Tax.__init__(self, 0.1);
		
class ImportDutyTax(Tax):
	def __init__(self):
		Tax.__init__(self, 0.05);

class Item(object):
	def __init__(self, iname, iprice, iamount, iimported, icat):
		self.name = iname
		self.price = iprice
		self.amount = iamount
		self.imported = iimported
		self.category = icat
		self.tax = None
			
	def calc(self):
		dt = ImportDutyTax()
		bt = BasicTax()
		if self.category == ItemCategory.OTHER:
			if self.imported == ItemOrigin.IMPORTED:
				self.tax = dt.taxitem(self.price) + bt.taxitem(self.price)
				self.price = bt.round_to((self.price + self.tax) * self.amount, 0.005)
			else:
				self.tax = bt.taxitem(self.price)
				self.price = bt.round_to((self.price + self.tax) * self.amount, 0.005)
		elif self.category == ItemCategory.BOOK or ItemCategory.FOOD or ItemCategory.MEDICAL:
			if self.imported == ItemOrigin.IMPORTED:
				self.tax = dt.taxitem(self.price)
				self.price = dt.round_to((self.price + self.tax) * self.amount, 0.005)
			else:
				self.tax = 0
				self.price = dt.round_to((self.price) * self.amount, 0.005)
								
class Receipt(object):
	def __init__(self):
		self.mylist = []
	
	def addtolist(self, litem):
		if type(litem) is Item:
			litem.calc()
			self.mylist.append(litem)
		else:
			return False
	
	def printlist(self):
		alltaxes = 0
		totalprice = 0
		for l in self.mylist:
			print ('{0} {1}: {2:.2f}' . format(l.amount, l.name, l.price))
			alltaxes = alltaxes + l.tax
			totalprice = totalprice + l.price
		print("Sales Taxes: {0:.2f}" . format(Tax.round_to(alltaxes, 0.05)))
		print("Total: {0:.2f}" . format(Tax.round_to(totalprice, 0.005)))	

class ItemOrigin(Enum):
	LOCAL = 1
	IMPORTED = 2
	
class ItemCategory(Enum):
	BOOK = 1
	FOOD = 2
	MEDICAL = 3
	OTHER = 4	

foods = ["chocolate bar", "box of chocolates", "box of chocolates", "imported box of chocolates", "chocolates"]
books = ["book"]
medical = ["headache", "pill", "headache pills", "packet of headache pills", "packet of headache pills"]

if __name__ == "__main__":	
	r = Receipt()
	f = open(sys.argv[1], 'r')
	lines = f.read().splitlines()

	for i in range(0, len(lines), 1):
		line = lines[i:i+1]
		line_list = [el.strip().split() for el in line]
		print(line_list)
		item = [it for sblist in line_list for it in sblist]
		print(item)
		tmp_item = (" ").join(item)
		print(tmp_item)
		item_qty = item[0]
		if "imported" in item:
			if  "box of imported" in tmp_item:
				tmp_name = item[1:item.index("at")]
				a, b = tmp_name.index("box"), tmp_name.index("imported")
				tmp_name[b], tmp_name[a] = tmp_name[a], tmp_name[b]
				c, d = tmp_name.index("of"), tmp_name.index("box")
				tmp_name[d], tmp_name[c] = tmp_name[c], tmp_name[d]
				item_name = (" ").join(tmp_name).strip()
				item_origin = ItemOrigin.IMPORTED
			else:
				tmp_name = item[1:item.index("at")]
				item_name = (" ").join(tmp_name).strip()
				item_origin = ItemOrigin.IMPORTED
		else:
			tmp_name = item[1:item.index("at")]
			item_name = (" ").join(tmp_name).strip()
			item_origin = ItemOrigin.LOCAL	
		
		if item_name in foods:
			item_cat = ItemCategory.FOOD
		elif item_name in books:
			item_cat = ItemCategory.BOOK
		elif item_name in medical:
			item_cat = ItemCategory.MEDICAL
		else:
			item_cat = ItemCategory.OTHER
			
		tmp_price = item[item.index("at")+1:]
		item_price = float(('').join(tmp_price))
	
		i1 = Item(item_name, item_price, int(item_qty), item_origin, item_cat)
		r.addtolist(i1)
	
	r.printlist()
