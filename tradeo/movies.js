/*
 * External js, wrapping up the logic for the task.
 */

/* This function is invoked before the body is reloaded to save
 * the current state of the table; After refresh of the page,
 * the body should look as before the refresh.
 */
function saveData() {
    "use strict";
    if(typeof(Storage) !== "undefined") {
        localStorage.clear();
        var tableData = $("#myTable tr");
        for (var i = 0; i < tableData.length; i += 1) {
            localStorage.setItem(i, tableData[i].innerHTML);
        }
    } else {
        window.alert("Your browser does not support local storage");
    }
}

/* Helper entity, representing the movie as object with
 * name, description. The 'type' attribute is used for properly
 * populating the table. It can be 'th' (header of the section) or
 * 'td' - the movie entry.
 */
var movieEntity = function (name, desc, type) {
    this.name = name;
    this.desc = desc;
    this.type = type;
};

/* The following function populates the movie database with
 * the movie name, description and the additional type (header or entry)
 * for the table.
 */
var initDb = function () {
    var movieList = [];
    movieList.push(new movieEntity("Любими", undefined, "TH"));
    movieList.push(new movieEntity("Всички", undefined, "TH"));
    movieList.push(new movieEntity("Armageddon (1998)", "After discovering that an asteroid the size of Texas is going to impact Earth in less than a month, N.A.S.A. recruits a misfit team of deep core drillers to save the planet.", "TD"));
    movieList.push(new movieEntity("Heat (1995)", "A group of professional bank robbers start to feel the heat from police when they unknowingly leave a clue at their latest heist.", "TD"));
    movieList.push(new movieEntity("Pi (1998)", "A paranoid mathematician searches for a key number that will unlock the universal patterns found in nature.", "TD")); 
    movieList.push(new movieEntity("Shreck (2001)", "After his swamp is filled with magical creatures, an ogre agrees to rescue a princess for a villainous lord in order to get his land back.", "TD"));
    return movieList;
}

/* The following function populates the table dynamically.*/
var initTable = function(movieDb) {
    $('body').append("<table></table>");
    $("table").attr("id", "myTable");
    for (var i = 0; i < movieDb.length; i += 1) {
        if (movieDb[i].type === "TH") {
            if (movieDb[i].name === "Любими") {
                $("table").append("<tr><th id=hfav>" + movieDb[i].name + "</th></tr>");
            } else if (movieDb[i].name === "Всички") {
                $("table").append("<tr><th id=hall>" + movieDb[i].name + "</th></tr>");
            } else {
                $("table").append("<tr><th>" + movieDb[i].name + "</th></tr>");
            }
        } else if(movieDb[i].type === "TD") {
            $("table tr:last").after("<tr><td><span>&#9734;</span><span class='name'>" + movieDb[i].name + "</span></td></tr>");
        }
    }
};

/* 
 * This function is used to extend the String object with
 * method to remove whitespaces.
*/
var extendString = function() {
    if(typeof(String.prototype.trim) === "undefined") {
        String.prototype.trim = function() {
            return String(this).replace(/^\s+|\s+$/g, '');
        };
    }
}

/* 
 * Main application object, containing init function, which is invoked
 * as method patern when the html body is loaded.
 */ 
var myApp = {
    init: $(document).ready(function() {
        var sectionFlag; // helper flag to indicate where the mouse is
        var hAll;		// represents the table header of the "all movies section"
        var hAll_index; // represents the index of the "all movies section" as position in the table
        var hAll_index_before; // represents the index before we reorder movies
        var hAll_index_after; // reprsents the index after we reorder movies
        var selector;	// jQuery selector

        var movieDb = initDb();
        extendString();

        /*
         * Check if the localStorage is empty and create and populate
         * a table with the movieDb content. If not - 
         * create and populate a table with localStorage content.
         */
        if (localStorage.length !== 0) {
            $('body').append("<table></table>");
            $("table").attr("id", "myTable");
            for (var i = 0; i < localStorage.length; i += 1) {
                $("table").append("<tr>" + localStorage.getItem(i) + "</tr>");
            }
        } else {
            movieDb = initTable(initDb());
        }

        /*
         * When we click on movie name - display popup 
         * with movie's description. On double click - close the popup.
         */
        $(".name").click(function (e) {
            if (!document.getElementById("tooltip")) {
                var tooltip = document.createElement("p");
                var body = document.getElementsByTagName("body")[0];
                for (var i = 0; i < movieDb.length; i += 1) {
                    var movie = $(this).text().trim();
                    if (movie === movieDb[i].name) {
                        tooltip.innerHTML = movieDb[i].desc;
                        tooltip.setAttribute("id", "tooltip");
                    }
                }
                $(tooltip).css("position", "absolute");
                $(tooltip).css({"left": e.pageX, "top": e.pageY});
                body.appendChild(tooltip); // show the popup where the mouse is
                $(tooltip).dblclick(function() {
                    $(this).remove();
                });
            }
        });

        /*
         * When we click on empty star - append the movie to the
         * "favourite section". When we click on filled star - 
         * append the movie to the "all movies section"
         */ 
        $("span").not(".name").hover(function() {
            if ($(this).html() === String.fromCharCode(9733)) {
                $(this).html("&#9734");
                sectionFlag = 1;
            } else if ($(this).html() === String.fromCharCode(9734)) {
                $(this).html("&#9733");
                sectionFlag = 0;
            }
        }).click(function() {
            var thisTr = $(this).parents("tr");

            if (sectionFlag === 0) {
                $(thisTr).find("span:eq(0)").html("&#9734;");
                hAll = $("#hall").parents("tr");
                hAll_index = $("tr").index(hAll[0]);
                selector = "tr:eq(" + hAll_index.toString() + ")";
                $(selector).before(thisTr[0]);
            } else if (sectionFlag === 1) {
                $(thisTr).find("span:eq(0)").html("&#9733;");
                hAll = $("#hall").parents("tr");
                hAll_index = $("tr").index(hAll[0]);
                selector = "tr:last";
                $(selector).after(thisTr[0]);
                hAll_index = $("tr").index(hAll[0]);
                var allMovies = $("tr").slice(hAll_index+1);
                allMovies = allMovies.toArray();

                $(allMovies).sort(function (a, b) {
                    var aText = $(a).find('.name').text().trim();
                    var bText = $(b).find('.name').text().trim();
                    var res = aText.localeCompare(bText);
                    if (res > 0) {
                        $(a).insertAfter($(b));
                    } else if (res < 0) {
                        $(b).insertAfter($(a));
                    } else {
                        ; // do nothing
                    }
                    return res;
                });
            } else {
                ; // do nothing
            }
        });

        /* Using the jQuery UI and its sortable widget, we could sort
         * movies in the "favourite movies section" as we wish
         */
        $("#myTable tbody").sortable({
            helper: function (e, ui) {	// fix the width when we pick an element
                var $originals = ui.children();
                var $helper = ui.clone();	
                $helper.children().each(function(index) {
                    $(this).width($originals.eq(index).width())
                });
                return $helper;
            },
            stop: function (e, ui) { // perform checks if we move an unallowed element (section headers and movies from the "all movies section"
                var hFav = $("#hfav").parents("tr");
                var hFav_index = $("tr").index(hFav[0]);
                var currentIndex = $("tr").index(ui.item);

                if (currentIndex > hAll_index_after ||
                        currentIndex === hAll_index_after ||
                        currentIndex < hFav_index) {
                    $("#myTable tbody").sortable("cancel");
                }
                if (hAll_index_before > hAll_index_after) {
                    $("#myTable tbody").sortable("cancel");
                }

                var curent = $('td.index', ui.item.parent()).each(function (i) {
                    $(this).html(i + 1);
                });
                return curent;
            },
            start: function (e, ui) {
                var hAll = $("#hall").parents("tr");
                hAll_index_before = $("tr").index(hAll[0]);
            },
            change: function (e, ui) { // perform checks if we move an unallowed element (section headers and movies from the "all movies section"
                var currentIndex = $("tr").index(ui.item);
                var hFav = $("#hfav").parents("tr");
                var hFav_index = $("tr").index(hFav[0]);
                var hAll = $("#hall").parents("tr");
                hAll_index_after = $("tr").index(hAll[0]);

                if (hAll_index_before < currentIndex ||
                        currentIndex === hAll_index_after ||
                        currentIndex === 0 ||
                        currentIndex === hFav_index) {
                    $("#myTable tbody").sortable("cancel");
                }
                var hAll = $("#hall").parents("tr");
                hAll_index_after = $("tr").index(hAll[0]);
            }
        }).disableSelection();
    })
};
