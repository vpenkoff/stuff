/* Simplest http server for educational purposes */

var http = require('http');
var fs = require('fs');
var path = require('path');
/*
server = http.createServer(function (request, response) {
	console.log(request.headers);
	console.log(request.method);

});
*/
server = http.createServer();
server.on('connection', function(socket) {
	console.log('someone connected');
	console.log(socket.address());
});
server.on('request', function(request, response) {
	var filePath = "." + request.url;
	
	console.log(request.headers);
	console.log(request.method);
	console.log(request.url);

	if (filePath == "./") {
		filePath = "index.html";
	}

	path.exists(filePath, function(exists) {
		if (exists) {
			fs.readFile(filePath, function(error, content) {
				if (error) {
					response.writeHead(500);
					response.end;
				} else {
					response.writeHead(200, { 'Content-Type': 'text/html' });
					response.end(content, 'utf-8');
				}
			});
		} else {
				response.writeHead(404);
				response.end();
		}
	});
});
server.listen(8080);
