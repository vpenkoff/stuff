class Item(object):
    id = 0 
    def __init__(self, name = "", cost = 0):
        Item.id = Item.id + 1
        self.__id = Item.id
        self.__name = name
        self.__cost = cost

    def getId(self):
        return self.__id

    def getName(self):
        return self.__name

    def getCost(self):
        return self.__costa

    def printItem(self):
        return "{0}, {1}, {2}".format(self.__id, self.__name, self.__cost)

class ItemList(object):
    def __init__(self):
        self.__item_list = []

    def item_add(self):
        name = raw_input("Please enter a name for the item: ")
        cost = raw_input("Please enter a cost for the item: ")
        self.__item_list.append(Item(name, cost))

    def item_del(self):
        id = input("Please enter the id which is to be removed: ")
        for i in self.__item_list:
            if i.id == id:
                self.__item_list.remove(i)
            else:
                print "No such item"

#    def item_show(self):
#        for i in self.__item_list:
#            i.printItem()

    def item_save(self):
        pass

    def item_get(self):
        return self.__item_list


class ItemFile(object):
    def __init__(self):
        self.__f = None

    def file_open_r(self):
        self.__f = open('data.txt', 'r')
        for line in self.__f:
            print line

    def file_open_a(self):
        self.__f = open('data.txt', 'a')

    def file_close(self):
        self.__f.close()

    def file_append_items(self, itemlist):
        for i in itemlist:
            self.__f.write(str(i.printItem()))

if __name__ == "__main__":
    print "Welcome to pyfin!"
    item_list = ItemList()
    item_file = ItemFile()
    while True:
        print "Please select an option: "
        print "For adding a new item press a"
        print "For deleting a new item press d"
        print "For listing current items press l"
        print "For saving to disk press s"
        print "To quit press q\n"

        i = raw_input()

        if i == 'a':
            item_list.item_add()
        elif i == 'd':
            item_list.item_del()
        elif i == 'l':
            item_file.file_open_r()
            item_file.file_close()
        elif i == 's':
            item_file.file_open_a()
            item_file.file_append_items(item_list.item_get())
            item_file.file_close()
        elif i == 'q':
            break
        else:
            print "Invalid input"
